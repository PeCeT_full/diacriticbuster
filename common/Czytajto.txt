Diacritic Buster (Pogromca znaków diakrytycznych) #TUPODAJWERSJĘ
Autor: PeCeT_full
Strona WWW: http://www.komputermania.pl.eu.org/pl
Copyright © by PeCeT_full 2015. Diacritic Buster jest publikowany na zasadach licencji MIT. Więcej informacji można uzyskać zapoznając się z plikiem Licence.txt dołączonym do aplikacji (polskie tłumaczenie licencji MIT: http://blaszyk-jarosinski.pl/wp-content/uploads/2008/05/licencja-mit-tlumaczenie.pdf).

W razie wszelkich problemów czy też wątpliwości proszę o kontakt.

-------------
Opis programu
-------------

Program ten może pomóc Tobie w pozbyciu się niekompatybilnych znaków znajdowanych w jakimkolwiek tekście używając w zamian ich alternatywnych lub uproszczonych form, aby były przetwarzalne przez dowolne docelowe oprogramowanie odczytujące nie będące sobie w stanie poradzić z ich oryginalnymi odpowiednikami. Został on napisany w języku C# i korzysta z interfejsu Windows Forms jako interfejsu użytkownika. Uruchamia się w trybie 32- lub 64-bitowym w zależności od środowiska systemowego.

Minimalne wymagania sprzętowe i systemowe: procesor o mocy 400 MHz lub szybszy; 96 MB dostępnej pamięci RAM; 1 MB dostępnego wolnego miejsca na dysku, w którym znajduje się program; system operacyjny Windows 98 lub nowszy z zainstalowaną platformą Microsoft .NET Framework 2.0.

----------------
Obsługa programu
----------------

Diacritic Buster jest prosty w użyciu – po prostu skopiuj i wklej tekst przeznaczony do konwersji do pola „Tekst pierwotny” (użyj kombinacji Ctrl+V albo naciśnij prawym przyciskiem myszy na pole tekstowe i wybierz polecenie „Wklej” z menu kontekstowego) i kliknij na „Konwertuj”. Ostateczny tekst pojawi się na prawo. Aby skopiować cały tekst, naciśnij prawym przyciskiem myszy na pole oraz wybierz opcję „Zaznacz wszystko” z menu, a następnie tym samym przyciskiem kliknij w tym samym miejscu i wybierz polecenie „Kopiuj”. Możesz teraz powrócić do swojego ulubionego edytora tekstowego albo gdziekolwiek indziej i kontynuować swą pracę zaraz po wklejeniu poprawionego kawałka tekstu.

Jeśli są znaki, które chciałbyś pozostawić w stanie nienaruszonym, możesz skorzystać z tzw. pliku schematu dopasowanego do Twoich potrzeb – zostaną przerobione tylko znaki wspomniane w takowym pliku.

--------------
Opcje programu
--------------

Diacritic Buster pozwala na zmianę następujących ustawień: 

* Konwertuj zawartość Schowka automatycznie za pomocą kombinacji Alt+C – dopóki wewnątrz Schowka systemowego znajduje się tekst, może on zostać skonwertowany za pomocą obecnie wybranego schematu. Pozwala to na korzystanie z programu bez potrzeby przełączania się pomiędzy aplikacjami.

* Program ukryty po uruchomieniu – po uruchomieniu program schowany jest w zasobniku systemowym. By go przywrócić, należy kliknąć dwukrotnie lewym przyciskiem myszy na ikonę programu albo nacisnąć prawym przyciskiem myszy i z menu kontekstowego wybrać polecenie „Ukryj/przywróć”.

* Dostępne schematy – lista schematów, z których można skorzystać. Te poniższe są oryginalnie dołączone do programu, a zatem gotowe do użycia: 
	— Alternate German accents (zastosuj alternatywne akcenty niemieckie), 
	— Convert Romaji text to Katakana, 
	— Make Hungarian compatible with Western European set (zapewnij zgodność języka węgierskiego z zachodnioeuropejskim zestawem znaków), 
	— Remove all Central European specific accents only (usuń wszystkie akcenty charakterystyczne tylko dla Europy Środkowej), 
	— Remove Polish accents except o-acute (usuń polskie akcenty oprócz liter O ze znakiem akcentu silnego), 
	— Remove Polish accents (usuń polskie akcenty), 
	— Romanise Katakana letters, 
	— Romanise Russian letters the GOST 2002(B) method (zromanizuj litery rosyjskie metodą GOST 2002(B)).
Gdy schemat jest ustawiony jako domyślny, program postara obejść się ze wszystkimi obecnymi znakami diakrytycznymi po swojemu. By dodać kolejny własny schemat do listy, będziesz musiał utworzyć nowy zwykły dokument tekstowy (tj. plik tekstowy napisany w prostym edytorze tekstów, takim jak Notatnik z systemu Windows, gedit czy vi nie posiadających żadnych funkcji formatujących) oraz koniecznie zapisać go jako plik o rozszerzeniu „.txt”, w kodowaniu znaków UTF-8. Potencjalny plik schematu programu Diacritic Buster musi składać się wyłącznie z linijek korzystających z następującego wzoru: „znak_lub_zbiór_znaków_do_zamiany|docelowy_znak_lub_zbiór_znaków” (bez cudzysłowia). Jeżeli coś jest nie tak z importowanym plikiem, nie zostanie on przyjęty, jednakże przekształcanie symboli (z wyjątkiem znaku „|”) w jakiekolwiek inne znaki również jest możliwe. Aby uczynić pracę z dwuznakami oraz dłuższymi kombinacjami w pełni możliwą, wymagane jest posortowanie malejąco wszystkich kluczy w pliku schematu rozpoczynając od najdłuższych kluczy (składających się z największej ilości znaków) do najkrótszych (składających się tylko z jednego znaku).

* Język – możesz przełączyć z obecnego języka interfejsu na jeden z pożądanych.
