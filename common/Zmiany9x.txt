Spis zmian programu Diacritic Buster

1.3.x.x (#TUPODAJDAT�WYDANIA): 
* usprawniono zachowanie si� przycisku OK w oknie opcji.

1.3.5580.33937 (12.IV.2015): 
* zdanie �Program hidden on start-up� zmieniono na �Program hidden on launch�; 
* poprawiona kolejno�� tabulacji w oknie opcji; 
* naprawiono problem z wy�wietlaniem okna opcji wyst�puj�cym, kiedy program by� ustawiony na bycie ukrytym po uruchomieniu.

1.2.5551.22763 (14.III.2015): 
* poprawiono schemat �Convert Romaji text to Katakana�; 
* usuni�to b��d skr�tu klawiaturowego Alt+C z brakuj�cym tekstem w Schowku; 
* pojawia si� stosowne okno z b��dem, gdy plik DiacriticBuster.ini nie mo�e by� utworzony ani nadpisany; 
* symbol kratki (ang. hash) zamiast znaku dolara jako warto�� w plikach schematu do wykonywania umownych zada� konwertowania; 
* pojawia si� stosowne okno z ostrze�eniem, kiedy klucze w pliku schematu powtarzaj� si� przynajmniej raz; 
* prosty test weryfikuj�cy podczas importowania schematu, czy kodowanie pliku to ANSI (gdy plik zawiera jakikolwiek znak diakrytyczny lub inny z r�norodnych system�w pisma).

1.1.5544.20372 (7.III.2015): 
* program pojawia si� w zasobniku systemowym � mo�na go tak�e ukry� po jego uruchomieniu; 
* mo�liwo�� konwersji zawarto�ci Schowka za pomoc� kombinacji klawiszy Alt+C; 
* obs�uga klucz�w wieloznakowych (typu string) � pozwala na prac� z dwuznakami oraz d�u�szymi kombinacjami; 
* za��czono schematy katakany; 
* usuni�to b��d z powtarzaj�cymi si� kluczami w plikach schematu; 
* poprawiono schemat �Romanise Russian letters the GOST 2002(B) method�; 
* zmieniono licencj� oprogramowania na licencj� MIT.

1.0.5479.35147 (1.I.2015) � pierwsza publicznie wydana wersja.
