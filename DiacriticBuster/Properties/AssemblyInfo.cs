﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Diacritic Buster")]
[assembly: AssemblyDescription("Transforms diacritical chars in a piece of text into their close equivalents")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("PeCeT_full")]
[assembly: AssemblyProduct("Diacritic Buster")]
[assembly: AssemblyCopyright("Copyright © PeCeT_full 2015")]
[assembly: AssemblyTrademark("Some Rights Reserved")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("eb792ef1-0042-411e-8280-080276c97da9")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.3.*")]
[assembly: AssemblyFileVersion("1.3.0.0")]
[assembly: NeutralResourcesLanguageAttribute("")]
